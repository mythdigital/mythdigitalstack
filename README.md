Project
=======

### 1\. Clone project setup files

`cd /Applications/MAMP/htdocs/project-name`

`git clone https://mythdigital@bitbucket.org/mythdigital/mythdigitalstack.git`

### 2\. Install Craft Project \(if required\)

`cd /Applications/MAMP/htdocs/project-name`

`composer create-project -s RC craftcms/craft`

### 3\. Install NPM Dependencies + GULP

`npm install`

### 4\. Run GULP

`gulp watch`


* * *


GIT Deploy Setup
================

### 1\. Login to VPS via SSH:

`ssh user@185.116.214.198`

### 2\. Run shell script in user root to create git hooks:

`bash vpsgithooks.sh`

Local GIT Deploy Setup
================

### 4\. Create local GIT deploy scripts

Change all 'user' below to match user on VPS

`git remote add site ssh://user@185.116.214.198/home/user/repo/site.git`


### 5\. To deploy changes, to production server

`git push site --branch--`


* * *


Notes
=========

### 1/. Dump database

```bash
$ /Applications/MAMP/Library/bin/mysqldump -u root -p database > database.sql
# Password
root
```

