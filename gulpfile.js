// Gulp4
var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var minifyCss= require('gulp-clean-css');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var newer = require('gulp-newer');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var exit = require('gulp-exit');
var minify = require('gulp-minify');
var merge = require('merge-stream');
var order = require('gulp-order');
var autoprefixer = require('gulp-autoprefixer');

var paths = {
  styles: {
    sass: 'assets/scss/**/*.scss',
    src: 'assets/scss/style.scss',
    dest: 'web/css'
  },
  scripts: {
    src:'assets/js/**/*.js',
    dest: 'web/js'
  },
  images: {
    src: 'assets/img/**/*',
    dest: 'web/img'
  },
  fonts: {
	src: 'assets/fonts/*',
	dest: 'web/fonts'
  },
  // Add additional vendor tasks
  vendor: {
	  scripts: [
        'node_modules/raf/index.js'
	  ],
	  fonts: [
	  	'node_modules/font-awesome/fonts/*'
	  ]
  }
};

function styles() {
  return gulp.src(paths.styles.src, {
      sourcemaps: true
    })
	.pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
	.pipe(rename({
	  basename: 'main',
	  suffix: '.min'
	}))
	.pipe(gulp.dest(paths.styles.dest));
}

function scripts() {
  return gulp.src(paths.scripts.src, {
		sourcemaps: true
	})
	.pipe(uglify())
	.pipe(babel({
        presets: ['env']
    }))
    .pipe(order([
	    "plugins/*.js",
	    "*.js",
	    "main.js"
	]))
	.pipe(rename({
	  basename: 'main'
	}))
	.pipe(minify())
	.pipe(gulp.dest(paths.scripts.dest));
}

function images() {
  return gulp.src(paths.images.src)
	.pipe(newer(paths.images.dest))
	.pipe(imagemin())
	.pipe(gulp.dest(paths.images.dest));
}

function fonts() {
  return gulp.src(paths.fonts.src)
	.pipe(gulp.dest(paths.fonts.dest))
}


// Vendor stuffs
function vendorScripts() {
  return gulp.src(paths.vendor.scripts)
	.pipe(uglify())
	.pipe(babel({
        presets: ['env']
    }))
	.pipe(rename({
	  basename: 'vendor'
	}))
	.pipe(minify())
	.pipe(gulp.dest(paths.scripts.dest));
}

function vendorFonts() {
  return gulp.src(paths.vendor.fonts)
	.pipe(gulp.dest(paths.fonts.dest))
}

function watch() {
  gulp.watch(paths.styles.sass, styles);
  gulp.watch(paths.scripts.src, scripts);
  gulp.watch(paths.images.src, images);
  gulp.watch(paths.fonts.src, fonts);
  gulp.watch(paths.vendor.scripts, vendorFonts);
  gulp.watch(paths.vendor.fonts, vendorScripts);
}

// Default tasks
gulp.task('watch', gulp.series(styles, scripts, images, fonts, vendorFonts, vendorScripts, 
  gulp.parallel(watch)
));
gulp.task('build', gulp.parallel(styles, scripts, images, fonts, vendorFonts, vendorScripts));